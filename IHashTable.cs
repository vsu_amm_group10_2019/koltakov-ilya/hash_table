﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NEW
{
    interface IHashTable
    {
        bool Add(Student student);
        Student Find(string id);
        void Delete(string id);
        List<Student> GetData(); 
        void Clear();
    }
}
