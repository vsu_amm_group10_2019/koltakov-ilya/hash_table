﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NEW
{
    public partial class AddForm : Form
    {
        public AddForm(FormState formState,Student student = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.Add: return;
                case FormState.Display:
                    {
                        fio.ReadOnly = true;
                        id.ReadOnly = true;
                        zachet.ReadOnly = true;
                        exam.ReadOnly = true;
                        dataGridViewZachet.ReadOnly = true;
                        dataGridViewExam.ReadOnly = true;
                        fio.Text = student.FIO;
                        id.Text = student.ID;
                        zachet.Text = Convert.ToString(student.Sessia.Tests.Count);
                        exam.Text = Convert.ToString(student.Sessia.Exams.Count);
                        for (int i = 0; i < student.Sessia.Tests.Count; i++)
                        {
                            dataGridViewZachet.Rows[i].Cells[0].Value = student.Sessia.Tests[i].Name;
                            dataGridViewZachet.Rows[i].Cells[1].Value = student.Sessia.Tests[i].Mark;
                        }
                        for (int i = 0; i < student.Sessia.Exams.Count; i++)
                        {
                            dataGridViewExam.Rows[i].Cells[0].Value = student.Sessia.Exams[i].Name;
                            dataGridViewExam.Rows[i].Cells[1].Value = student.Sessia.Exams[i].Mark;
                        }
                        break;
                    }
                case FormState.Change: 
                {
                        id.ReadOnly = true;
                        id.Text = student.ID;
                        fio.Text = student.FIO;
                        zachet.Text = Convert.ToString(student.Sessia.Tests.Count);
                        exam.Text = Convert.ToString(student.Sessia.Exams.Count);
                        for (int i = 0; i < student.Sessia.Tests.Count; i++)
                        {
                            dataGridViewZachet.Rows[i].Cells[0].Value = student.Sessia.Tests[i].Name;
                            dataGridViewZachet.Rows[i].Cells[1].Value = student.Sessia.Tests[i].Mark;
                        }
                        for (int i = 0; i < student.Sessia.Exams.Count; i++)
                        {
                            dataGridViewExam.Rows[i].Cells[0].Value = student.Sessia.Exams[i].Name;
                            dataGridViewExam.Rows[i].Cells[1].Value = student.Sessia.Exams[i].Mark;
                        }

                        break;
                }
                case FormState.Delete:
                case FormState.Search: 
                    {
                        fio.ReadOnly= true;
                        zachet.ReadOnly = true;
                        exam.ReadOnly = true;
                        break;
                    }
                
            }
                
        }

        public Student student1 { get; } = new Student();
        public Session session = new Session();
        public bool OK = false;
        private FormState FormState;

        private void buttonOK_Click(object sender, EventArgs e)
        {
            student1.ID = id.Text;

            if (FormState == FormState.Add || FormState == FormState.Change)
            {
                student1.Sessia = session;
                student1.FIO = fio.Text;

                for (int i = 0; i < dataGridViewZachet.RowCount; i++)
                {
                    student1.Sessia.Tests.Add(new Test());
                    student1.Sessia.Tests[i].Name = dataGridViewZachet.Rows[i].Cells[0].Value.ToString();
                    student1.Sessia.Tests[i].Mark = Convert.ToBoolean(dataGridViewZachet.Rows[i].Cells[1].Value);
                }
                for (int j = 0; j < dataGridViewExam.RowCount; j++)
                {
                    student1.Sessia.Exams.Add(new Exam());
                    student1.Sessia.Exams[j].Name = dataGridViewExam.Rows[j].Cells[0].Value.ToString();
                    student1.Sessia.Exams[j].Mark = Convert.ToInt32(dataGridViewExam.Rows[j].Cells[1].Value);
                }
            }
            OK = true;
            Close();
        }

        private void zachet_TextChanged(object sender, EventArgs e)
        {
            dataGridViewZachet.RowCount = Int32.Parse(zachet.Text);
        }

        private void exam_TextChanged(object sender, EventArgs e)
        {
            dataGridViewExam.RowCount = Int32.Parse(exam.Text);
        }
    }
}
