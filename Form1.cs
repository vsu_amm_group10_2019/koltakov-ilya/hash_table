﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NEW
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Student Student = new Student();
        LineHashTable hashTable = new LineHashTable(50);
        private string FileName { get; set; }

        public void Redraw()
        {
            List<Student> students = hashTable.GetData();
            dataGridView.Rows.Clear();
            dataGridView.RowCount = students.Count;
            for (int i=0; i<students.Count;i++) 
            {
                dataGridView.Rows[i].Cells[0].Value=students[i].ID;
                dataGridView.Rows[i].Cells[1].Value = students[i].FIO;
            }
        }

        private void menuAdd_Click(object sender, EventArgs e)
        {
            AddForm addform = new AddForm(FormState.Add);
            addform.ShowDialog();
            if (addform.OK)
            {
                if (hashTable.Add(addform.student1))
                    Redraw();
                else
                    MessageBox.Show("Не удалось добавить");
            }
        }

        private void menuSearch_Click(object sender, EventArgs e)
        {
            AddForm addform = new AddForm(FormState.Search);
            addform.ShowDialog();
            Student student = hashTable.Find(addform.student1.ID);
            

            AddForm displayform = new AddForm(FormState.Display, student);
            displayform.ShowDialog();
        }

        private void menuChange_Click(object sender, EventArgs e)
        {

            AddForm searchform = new AddForm(FormState.Search);
            searchform.ShowDialog();
            if (searchform.OK)
            {
                string id = searchform.student1.ID;
                Student student = hashTable.Find(id);

                AddForm changeform = new AddForm(FormState.Change, student);

                changeform.ShowDialog();
                if (changeform.OK)
                {
                    hashTable.Delete(id);
                    hashTable.Add(changeform.student1);
                    Redraw();
                }
            }
        }

        private void menuDelete_Click(object sender, EventArgs e)
        {
            AddForm deleteform = new AddForm(FormState.Search);
            deleteform.ShowDialog();
            if (deleteform.OK)
                hashTable.Delete(deleteform.student1.ID);
            
            Redraw();
        }
        /// <summary>
        /// /////////////////////////////////////////////////////////////////////
        /// </summary>
        private void SaveToFile()
        {
            List<Student> students = hashTable.GetData();
            string result = JsonConvert.SerializeObject(students);
            File.WriteAllText(FileName, result);
        }

        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            FileName = "";
            hashTable.Clear();
            Redraw();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Student> students = JsonConvert.DeserializeObject<List<Student>>(txt);
                hashTable.Clear();
                foreach (Student student in students)
                {
                    hashTable.Add(student);
                }
                Redraw();
            }
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                SaveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
