﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NEW
{
    public class LineHashTable:IHashTable
    {
        private Elem[] Table { get; }
        public int Size { get; }
        public LineHashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public bool Add(Student student)
        {
            if (Find(student.ID) != null)
            {
                return false;
            }
            int hash = Student.Hash(student.ID);
            int index = hash % Size;
            if (Table[index] == null || Table[index].Deleted) // Добавление в таблицу
            {
                Table[index] = new Elem()
                {
                    Student = student
                };
                return true;
            }
            for (int i = index + 1; i < Size; i++) // Метод решения коллизии
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Student = student
                    };
                    return true;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Student = student
                    };
                    return true;
                }
            }
            return false;
        }
        public Student Find(string id)
        {
            int hash = Student.Hash(id);
            int index = hash % Size;
            if (Table[index] == null)
            {
                return null; 
            }
            if (!Table[index].Deleted && Table[index].Student.ID == id)
            {
                return Table[index].Student;
            }
            for (int i = index + 1; i < Size; i++) 
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    return Table[i].Student;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    return Table[i].Student;
                }
            }
            return null;
        }
        public void Delete (string id)
        {
            int hash = Student.Hash(id);
            int index = hash % Size;
            if (Table[index] == null)
            {
                return;
            }
            if (!Table[index].Deleted && Table[index].Student.ID == id)
            {
                Table[index].Deleted = true;
                return;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    Table[i].Deleted=true;
                    return;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
        }


        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Student> GetData()
        {
            List < Student > list = new List<Student>();
            foreach(Elem e in Table)
            {
                if(e != null && !e.Deleted)
                {
                    list.Add(e.Student);
                }
            }
            return list;
        }
    }
    class Elem
    {
        public Student Student { get; set; }
        public bool Deleted { get; set; } = false;

    }
}
