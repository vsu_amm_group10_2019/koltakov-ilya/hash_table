﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEW
{
    public class Session
    {
        public List<Exam> Exams { get; set; } = new List<Exam>();
        public List<Test> Tests { get; set; } = new List<Test>();
    }
}
